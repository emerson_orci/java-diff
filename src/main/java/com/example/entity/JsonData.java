package com.example.entity;

/**
 * Class for creating a string Json for the rest controller
 * @author emersonr
 *
 */
public class JsonData {
	
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

		

}
